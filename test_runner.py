"""
Tests for the runner
"""
import os
import uuid
import pytest
import platform
from GitlabPyRunner.runner import Runner
from GitlabPyRunner.executor import run


GITLAB = "https://gitlab.com"


def register_runner(tags=[]):
    token = os.getenv("TEST_REGISTRATION_TOKEN", None)
    if not token:
        pytest.skip("TEST_REGISTRATION_TOKEN is not set")
    run = Runner(GITLAB, None)
    if not tags:
        tags = [str(uuid.uuid4())]
    run.register("test runner", token, tags=tags)
    assert run.token
    return run


@pytest.yield_fixture()
def random_runner():
    run = register_runner()
    yield run
    run.unregister()
    assert not run.token


@pytest.yield_fixture()
def playground_runner():

    tags = ["python-runner-new"]
    if platform.system() == "Windows":
        tags.append("python-runner-windows")
    if platform.system() == "Linux":
        tags.append("python-runner-linux")
    run = register_runner(tags=tags)
    yield run
    run.unregister()
    assert not run.token


def test_register(random_runner):
    """
    Test that we can register and unregister a runner
    :return:
    """
    assert random_runner.token


def test_poll_none(random_runner):
    """
    Poll for a job, we should not get one because we use a random runner tag
    :param random_runner:
    :return:
    """
    job = random_runner.poll()
    assert not job


def test_poll_manual(playground_runner):
    """
    Run a real job
    :param playground_runner:
    :return:
    """
    job = playground_runner.poll()
    assert job
    assert run(playground_runner, job)
    playground_runner.success(job)

