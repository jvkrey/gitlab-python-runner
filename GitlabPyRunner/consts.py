"""
Constants for gitlab-python-runner
"""
NAME = "gitlab-python-runner"
VERSION = "11.4.2-19"
USER_AGENT = "{} {}".format(NAME, VERSION)
